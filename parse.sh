width=400

Formula1='`TTL =  maxTime + 2 * stddev` <br>(no weight)'
Formula2='`TTL = w * reqTTL + (1 - w) * eTTL` <br>`eTTL = maxTime + 2 * stddev` <br> `w = 1 - 0.01 * numJobs, numJobs <= 100` <br> `w = 0, numJobs > 100` <br>(the weigth drops from 1 to 0)'
Formula3='`TTL = w * reqTTL + (1 - w) * eTTL` <br>`eTTL = maxTime + 2 * stddev` <br> `w = 1 - 0.009 * numJobs, numJobs <= 100` <br> `w = 0.1, numJobs > 100` <br>(the weigth drops from 100% to 10%)'
#Formula4='`TTL = w * reqTTL + (1 - w) * eTTL` <br>`eTTL = maxTime + 2 * stddev` <br> `w = 1 - 0.009 * numJobs, numJobs <= 100` <br> `w = 0.1 - (numJobs - 100) * 0.008, 100 < numJobs <= 200` <br>`w = 0.1, numJobs > 200` <br>(the weigth drops from 100% to 10% and from 10% to 2%)'
Formula4='`TTL = w * reqTTL + (1 - w) * eTTL` <br>`eTTL = maxTime + 2 * stddev` <br> `w = 10/(10 + numJobs)` <br>(the weigth becomes smaller while the number of jobs increases)'
Formula5='`TTL = w * reqTTL + (1 - w) * eTTL` <br>`eTTL = maxTime + 2 * stddev` <br> `w = 50/(50 + numJobs)` <br>(the weigth becomes smaller while the number of jobs increases)'

declare -A formulas
formulas["Formula1"]="z"
formulas["Formula2"]="z_ttl"
formulas["Formula3"]="z_ttl2"
formulas["Formula4"]="z_ttl10N"
formulas["Formula5"]="z_ttl50N"

echo "## Plot lines"
echo
echo '$`\textcolor{blue}{\text{blue line}}`$:`TTL = maxTime + 2 * stddev`, outliers are not removed'
echo
echo '$`\textcolor{yellow}{\text{yellow line}}`$:`TTL = maxTime + 2 * stddev`, outliers are removed using interquartile method (we must store all the data available)'
echo
echo '$`\textcolor{green}{\text{green line}}`$:`TTL = <given formula>`, outliers are removed using Z-score (based on current mean and stddev)'
echo

for f in $(ls 2_incremental_z_ttl10N)
do
	echo "## ${f}"

	echo "| ${Formula1} | ${Formula2} | ${Formula3} | ${Formula4} | ${Formula5} |"
	echo "| --------- | --------- | --------- | --------- | --------- |"
	for formula in $(echo "${!formulas[@]}" | sed 's/ /\n/g' | sort)
	do
		echo -n "| <img src='2_incremental_${formulas[${formula}]}/${f}' width='$width'> "
	done
	echo "|"
	echo

done

